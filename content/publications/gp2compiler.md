---
title: "Improving the GP2 Compiler"
date: 2019-09-23
pubtype: "Technical Report"
featured: true
authors: "G. Campbell, J. Romo, D. Plump"
description: "Department of Computer Science,
University of York, 49 pages, 2018."
tags: ["Graph Transformation","Theoretical CS", "Compilers"]
pdf: "/2019/Improving-the-GP2-Compiler.pdf"
bib: "/2019/Improving-the-GP2-Compiler.bib"
weight: 400
sitemap:
  priority : 0.8
---

GP 2 is an experimental programming language based on graph transformation rules which aims to facilitate program analysis and verification. Writing
efficient programs in such a language is hard because graph matching is expensive, however GP 2 addresses this problem by providing rooted rules which, under mild conditions, can be matched in constant time using the GP 2 to C compiler. In this report, we document various improvements made to the compiler; most notably the introduction of node lists to improve iteration performance for destructive programs, meaning that binary DAG recognition by reduction need only take linear time where the previous implementation required quadratic time.
