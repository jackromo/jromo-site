---
title: "Homotopy Bicategories of Complete 2-fold Segal Spaces"
date: 2023-11-21
pubtype: "Paper"
featured: true
authors: "J. Romo"
description: "School of Mathematics,
University of Leeds, 75 pages, 2023."
tags: ["Higher Category Theory","Category Theory", "Pure Mathematics"]
pdf: "/2023/Homotopy-Bicategories.pdf"
bib: "/2023/Homotopy-Bicategories.bib"
weight: 400
sitemap:
  priority : 0.8
---

In this paper, we address the construction of homotopy bicategories of (∞,2)-categories, which we take as being modeled by complete 2-fold Segal spaces. Our main result is the concrete construction of a functor h2 from the category of Reedy fibrant complete 2-fold Segal spaces (each decorated with chosen sections of the Segal maps) to the category of unbiased bicategories and pseudofunctors between them. Though our construction depends on the choice of sections, we show that for a given complete 2-fold Segal space, all possible choices yield the same unbiased bicategory up to an equivalence that acts as the identity on objects, morphisms and 2-morphisms. We illustrate our results with the example of the fundamental bigroupoid of a topological space.
