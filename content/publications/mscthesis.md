---
title: "Higher Artin Stacks and Moduli Stacks"
date: 2020-09-01
pubtype: "MSc Thesis"
featured: true
authors: "J. Romo"
description: "MSc Thesis, Department of Mathematics, University of Oxford, 144 pages, 2020."
tags: ["Algebraic Geometry", "Higher Category Theory", "Moduli Theory"]
pdf: "/2020/Higher-Artin-Stacks-And-Moduli-Stacks.pdf"
bib: "/2020/Higher-Artin-Stacks-And-Moduli-Stacks.bib"
weight: 400
sitemap:
  priority : 0.8
---

It is self-evident that in general, given a class of spaces A with some equivalence relation ~ of symmetries, we can expect its elements to vary in a geometric manner. One way to formalize this is to parameterize each element of A/~ by a point in some other space M, termed a moduli space. Unfortunately, should our spaces have non-trivial automorphisms, the relation ~ may be too convoluted for a moduli space to exist. A solution is instead to encode geometric structure in our original moduli problem, producing something akin to a sheaf which remembers symmetries. This yields a stack, which when considered in the context of algebraic geometry and given further geometric properties becomes an Artin stack, a construction rich enough to yield interesting geometric data but general enough to exist in a vast range of cases.

However, sometimes even this is insufficient: we may be interested not only in some spaces and their automorphisms, but also the higher automorphisms of these, ad infinitum. Furthermore, the relation ~ we choose may be induced by morphisms without inverses, such as quasi-isomorphisms or homotopy equivalences. In these cases, a notion of higher morphism is necessary, demanding we turn to higher category theory for aid. Such a line of inquiry leads us to Toën and Vezzosi's notion of a higher Artin stack, the correct setting for the problems posed above.

It is the goal of this dissertation to present the theory of higher Artin stacks. We begin with an overview of moduli spaces and the intertwined study of geometric invariant theory. We then proceed to classical stack theory, developing the standard notions of Grothendieck topology, fibred categories and descent data required. After this, we dive into higher category theory, exploring simplicial and Segal categories, model categories and localizations. Finally, we define a Segal category of higher stacks and higher Artin stacks, closing with some useful constructions like an étale cohomology for higher Artin stacks and some examples like stacks of abelian categories and stacks of perfect complexes.
